#
# Module: essioc
#
require essioc

#
# Module: vac_ctrl_tpg300_500
#
require vac_ctrl_tpg300_500


#
# Module: essioc
#
iocshLoad("${essioc_DIR}/common_config.iocsh")

#
# Device: Spk-030Crm:Vac-VEG-10001
# Module: vac_ctrl_tpg300_500
# Todo: Check if the port is correct
#
iocshLoad(${vac_ctrl_tpg300_500_DIR}/vac_ctrl_tpg300_500_moxa.iocsh, "DEVICENAME = Spk-030Crm:Vac-VEG-10001, IPADDR = moxa-vac-dtl-30-u006.tn.esss.lu.se, PORT = 4002")

#
# Device: Spk-030Crm:Vac-VGC-10000
# Module: vac_ctrl_tpg300_500
#
iocshLoad(${vac_ctrl_tpg300_500_DIR}/vac_gauge_tpg300_500_vgc.iocsh, "DEVICENAME = Spk-030Crm:Vac-VGC-10000, CHANNEL = A1, CONTROLLERNAME = Spk-030Crm:Vac-VEG-10001")

#
# Device: Spk-030Crm:Vac-VGC-20000
# Module: vac_ctrl_tpg300_500
#
iocshLoad(${vac_ctrl_tpg300_500_DIR}/vac_gauge_tpg300_500_vgc.iocsh, "DEVICENAME = Spk-030Crm:Vac-VGC-20000, CHANNEL = B1, CONTROLLERNAME = Spk-030Crm:Vac-VEG-10001")

#
# Check if need of disable memory locking with 'var dbThreadRealtimeLock 0'
