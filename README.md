# VacS-SpkCrm_SC-IOC-032

IOC for Spk-010Crm TPG Gauge Controller

---

## Used Module

IOC uses [e3-vac_ctrl_tpg300_500](https://gitlab.esss.lu.se/e3/wrappers/vac/e3-vac_ctrl_tpg300_500)

## Devices

**Controller:** 
- Spk-030Crm:Vac-VEG-10001

**Controlled Devices:**
- Spk-030Crm:Vac-VGC-10000
- Spk-030Crm:Vac-VGC-20000

